/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */

import chai from 'chai'
import {
  mult,
  add,
  sub,
  div,
  conwayNext } from '../src/math'
const should = chai.should()

describe('Math', () => {
  describe('Add', () => {
    xit('should add 2 numbers', () => {
      const res = add(6, 7)
      should.exist(res)
      res.should.equal(42)
    })

    xit('should add 2 decimals', () => {
      const res = add(3.14, 0.72)
      res.should.equal(3.14 + 0.72)
    })

    xit('should add several numbers', () => {
      const res = add(1, 2, 3, 4)
      res.should.equal(10)
    })
  })

  describe('Multiply', () => {
    xit('should multiply 2 numbers', () => {
      const res = mult(6, 7)
      should.exist(res)
      res.should.equal(42)
    })

    xit('should multiply several numbers', () => {
      const res = mult(6, 7)
      should.exist(res)
      res.should.equal(42)
    })
  })

  describe('Divide', () => {
    xit('should divide 2 numbers', () => {
      const res = div(10, 5)
      res.should.equal(2)
    })

    xit('should divide 2 decimals', () => {
      const res = div(10, 3)
      res.should.equal(10 / 3)
    })
  })

  describe('Substract', () => {
    xit('should substract 2 numbers', () => {
      const res = sub(6, 7)
      should.exist(res)
      res.should.equal(42)
    })
  })

  describe('Conway Sequence', () => {
    const valid = [
      '1',
      '11',
      '21',
      '1211'
    ]
    valid.map(elem => {
      xit('should give the next in the conway sequence for: ' + elem, () => {
        let res = conwayNext(elem)
        let next = conwayNext(res)
        res.length.should.be.above(2)
      })
    })

    const invalid = [
      '',
      '3',
      null,
      undefined
    ]
    invalid.map(elem => {
      xit('should give the next in the conway sequence for: ' + elem, () => {
        let res = conwayNext(elem)
        res.should.be.null
      })
    })
  })
})
